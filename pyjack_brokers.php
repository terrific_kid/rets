<?php
if ( ! function_exists('pyjack_brokers') ) {

    // Register Custom Post Type
    function pyjack_brokers() {

        $labels = array(
            'name'                  => _x( 'Brokers', 'Post Type General Name', 'text_domain' ),
            'singular_name'         => _x( 'Broker', 'Post Type Singular Name', 'text_domain' ),
            'menu_name'             => __( 'Brokers', 'text_domain' ),
            'name_admin_bar'        => __( 'Broker', 'text_domain' ),
            'archives'              => __( 'Broker Archives', 'text_domain' ),
            'attributes'            => __( 'Broker Attributes', 'text_domain' ),
            'parent_item_colon'     => __( 'Parent Broker:', 'text_domain' ),
            'all_items'             => __( 'Manage Advertising Brokers', 'text_domain' ),
            'add_new_item'          => __( 'Add New Broker', 'text_domain' ),
            'add_new'               => __( 'Add New', 'text_domain' ),
            'new_item'              => __( 'New Broker', 'text_domain' ),
            'edit_item'             => __( 'Edit Broker', 'text_domain' ),
            'update_item'           => __( 'Update Broker', 'text_domain' ),
            'view_item'             => __( 'View Broker', 'text_domain' ),
            'view_items'            => __( 'View Broker', 'text_domain' ),
            'search_items'          => __( 'Search Broker', 'text_domain' ),
            'not_found'             => __( 'Not found', 'text_domain' ),
            'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
            'featured_image'        => __( 'Featured Image', 'text_domain' ),
            'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
            'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
            'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
            'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
            'uploaded_to_this_item' => __( 'Uploaded to this Broker', 'text_domain' ),
            'items_list'            => __( 'Bsroker list', 'text_domain' ),
            'items_list_navigation' => __( 'Brokers list navigation', 'text_domain' ),
            'filter_items_list'     => __( 'Filter Brokers list', 'text_domain' ),
        );
        $args = array(
            'label'                 => __( 'Broker', 'text_domain' ),
            'description'           => __( 'Advertising Brokers', 'text_domain' ),
            'labels'                => $labels,
            'supports'              => array(''),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'   => 'pyjack',
            'menu_position'         => 25,
            'menu_icon'             => 'dashicons-businessman',
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => false,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => true,
            'publicly_queryable'    => true,
            'capability_type'       => 'page',
        );
        register_post_type( 'pyjack_brokers', $args );

    }
    add_action( 'init', 'pyjack_brokers', 0 );

}
var pyjack = angular.module('pyjack',['ui.bootstrap','slickCarousel']);

pyjack.factory('pdata', ['$http', function($http) {
	var factory = {};

			//Get Broker Listings
		 	factory.brokerListings = function(payload){
			 	var request = $http({
					                method: "POST",
					                url: "/wp-content/plugins/rets/json.php",
					                data: payload
	              });
			return request;
		 	}

return factory;
}]);
		
pyjack.controller('listing', function($rootScope, $scope, $location, $sce, pdata){
	
 
	
	$scope.brokerListings = function(brokerid){
		$scope.payload = {
			brokerid: brokerid
		}
		pdata.brokerListings($scope.payload).then(function (data) {
		 	$scope.blist = data.data;
		 	console.log('loaded listings');
		});
		
	}
	
	
	$scope.perPage = 5;
	$scope.total = 0;
	$scope.currentPage = 1;
	$scope.start = 0;
	
		$scope.listings = 'tk!';
		$scope.single = false;
		
		$scope.load = function(payload){
			$scope.listings = JSON.parse(decodeURIComponent(payload));
			$scope.total = $scope.listings.length;
			if($location.path()) $scope.loadItem($location.path());
		}
		
		$scope.viewItem = function(item, reload){
			window.location.href = '#!/' + item.LIST_105;
		}
		
		$scope.flip = function(page){
			$scope.currentPage = page;
			$scope.start = $scope.currentPage * $scope.perPage;
			
		}
		
		$scope.loadItem = function(path){
			$scope.single = false;
			console.log('Loading!');
			path = path.slice(1);
			angular.forEach($scope.listings, function(value, key) {
			if(value.LIST_105 == path) {
				$scope.single = value; 
				console.log($scope.single);
				
				$scope.term = 30;
				$scope.rate = 5.75;
				$scope.principal = $scope.single.LIST_22;
				$scope.down = 20;
				$scope.brokerListings($scope.single.LIST_106);
				
				}
			});
			
			
		}
		
		$scope.process = function(string){
			return string.replace(/[^\w\s]/gi, ' ');
		}
		window.onhashchange = function() {
			addthis.layers.refresh();
			$scope.$apply(function(){ 
				$scope.loadItem($location.path());
			});
			
		}
		
		$scope.safeUrl = function(url){
			return $sce.trustAsResourceUrl(url);
		}
		$scope.mapLink = function(single){
			$scope.mapUrl = 'https://www.google.com/maps/embed/v1/place?key=AIzaSyAj5CBbP5HpM_h37iXzAvmH5FIw3t957xE&q=';
			$scope.mapUrl += encodeURI($scope.process(single.LIST_34) + ',' + $scope.process(single.LIST_39) + ',' + $scope.process(single.LIST_43));
			return $sce.trustAsResourceUrl($scope.mapUrl);
		}
		
		
		$scope.calculate = function(){
			$scope.P = $scope.single.LIST_22 - ($scope.single.LIST_22 * ( $scope.down / 100) );
			$scope.i = $scope.rate / 100 / 12;
			$scope.n = $scope.term;
			$scope.payment = $scope.monthlyPayment($scope.P, $scope.n, $scope.i);
			
			console.log('calculated!');
			console.log($scope.P);
			console.log($scope.i);
			console.log($scope.n);
			console.log($scope.payment);
		}
		
		$scope.monthlyPayment = function(p, n, i) {
			 return p * i * (Math.pow(1 + i, n)) / (Math.pow(1 + i, n) - 1);
		}
 



		

});
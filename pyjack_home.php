<div class="container-fluid">
	<div class="row">
		
		<div style="margin-bottom: 1rem;" class="col-12">
			
				<a href="/all-listings/#!/<?php echo $gold['LIST_105']; ?>" style="color: black; text-decoration: none; border: 2px solid gold; padding:1rem; margin-bottom: 1rem;" class="row">
					<div style="min-height: 20vh; background: url('<?php echo $gold['img'][0]; ?>'); background-position: center center; background-size: cover;" class="col-sm-6">    
						 
					</div>
					<div class="col-sm-6">
						<ul class="pyjack-data">
        		<li><b>$<?php echo number_format($gold['LIST_22']); ?></b>
        		<li><?php echo $gold['LIST_31']; ?> <?php echo $gold['LIST_34']; ?>, <?php echo $gold['LIST_39']; ?>, <?php echo $gold['LIST_43']; ?></li>
        		</ul>
        		<div class="row">
            		<div class="col-6">
        		<ul class="pyjack-data">
            		<li ng-if="single.LIST_15"><b>Status:</b><br><?php echo $gold['LIST_15']; ?></li>
            		<li ng-if="single.LIST_105"><b>Type:</b><Br><?php echo $gold['LIST_105']; ?></li>
            		<li ng-if="single.LIST_66"><b>Beds:</b><br><?php echo $gold['LIST_66']; ?></li>
            		<li ng-if="single.LIST_67"><b>Baths:</b><br><?php echo $gold['LIST_67']; ?></li>
        		</ul></div>
        		
        		<div class="col-6">
        		<ul  class="pyjack-data">
            		<li ng-if="single.LIST_105"><b>MLS#:</b><br><?php echo $gold['LIST_105']; ?></li>
					 
            		<li ng-if="single.LIST_53"><b>Year Built:</b><br><?php echo $gold['LIST_53']; ?></li>
            		<li ng-if="single.LIST_48"><b>Square Footage:</b><br><?php echo $gold['LIST_48']; ?></li>
            		<li ng-if="single.LIST_56"><b>Lot Acres:</b><br><?php echo $gold['LIST_56']; ?></li>
            		
        		</ul>
        		</div>
        		
        		
        		</div>
        		
        		
  		</div>  
  		</a>
  		<div class="row">
  		<?php foreach($silver as $item){ ?>
  		
  		<a href="/all-listings/#!/<?php echo $item['LIST_105']; ?>" style="color: black; text-decoration: none;border: 2px solid #eee; padding: 1rem;" class="col-sm-4">
	  		<img src="<?php echo $item['img'][0]; ?>">
	  		<b>$<?php echo number_format($item['LIST_22']); ?></b>
	  		<p><?php echo $item['LIST_31']; ?> <?php echo $item['LIST_34']; ?><br> <?php echo $item['LIST_39']; ?></p>
	  		
	  		</a>
	  		
  		<?php }	  ?>
  	</div>
</div>
<?php
use phpFastCache\CacheManager;

 
// Setup File Path on your config files
CacheManager::setup(array(
    "path" => sys_get_temp_dir().'/phpFastCache', // or in windows "C:/tmp/"
));

// In your class, function, you can call the Cache
$cache = CacheManager::getInstance('files');





function pyjack_code_listing( $atts ){
    $options = shortcode_atts( array( 'class' => 'A' ), $atts );
    ob_start();
    $listings = pyjack_getListings($options['class']); 
    include(dirname(__FILE__).'/pyjack_rets_listing.php');
return ob_get_clean(); 
}


function pyjack_code_featured( $atts ){
    //$options = shortcode_atts( array( 'class' => 'A' ), $atts );
    $options = null;
    $gold = pyjack_gold();
    $silver = pyjack_silver();	
    ob_start();
    include(dirname(__FILE__).'/pyjack_home.php');
return ob_get_clean(); 
}

function pyjack_code_all( $atts ){
    //$options = shortcode_atts( array( 'class' => 'A' ), $atts );
    $options = null;
    $listings = pyjack_allListings();
    ob_start();
    include(dirname(__FILE__).'/pyjack_rets_listing.php');
return ob_get_clean(); 
}


function pyjack_queue() {
	wp_enqueue_style('bootstrap-grid', plugins_url( 'bootstrap-grid.css', __FILE__ ) );
	wp_enqueue_style('pyjack-css', plugins_url( 'pyjack.css', __FILE__ ) );
	
	wp_enqueue_style('slick-css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css' );
	wp_enqueue_style('slick-theme-css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css' );
	wp_enqueue_style('font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' );
	
	wp_enqueue_script('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-alpha1/jquery.min.js' );
	
	
	wp_enqueue_script('angular', 'https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.min.js' );
	wp_enqueue_script('angular-animate', 'https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular-animate.min.js' );
	wp_enqueue_script('angular-touch', 'https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular-touch.min.js' );
	wp_enqueue_script('ui-bootstrap', 'https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/2.5.0/ui-bootstrap.js' );
	wp_enqueue_script('ui-bootstrap-tpls', 'https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/2.5.0/ui-bootstrap-tpls.min.js' );
	
	wp_enqueue_script('slick', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js');
	wp_enqueue_script('angular-slick', plugins_url( '/angular-slick-carousel-master/dist/angular-slick.min.js', __FILE__ ) );
	
	
	
	wp_enqueue_script('pyjack-js', plugins_url( 'pyjack.js', __FILE__ ) );

}	

function pyjack_phrets(){
	 $config = new \PHRETS\Configuration;
	 $config->setLoginUrl('http://retsgw.flexmls.com:80/rets2_3/Login')
        ->setUsername('stj.rets.seaglass')
        ->setPassword('galls-ic68')
        ->setRetsVersion('1.7.2');
        	$rets = new \PHRETS\Session($config);
return $rets;
}

function pyjack_getAdvertisedBrokers(){
	 $brokers = array();
	 $query = new WP_Query( array( 'post_type' => 'pyjack_brokers' ) );
	 foreach($query->posts as $broker){
		 $fields = get_fields($broker->ID);
		 if($fields['advertised_broker'] == 1){
			 array_push($brokers, json_decode($fields['broker'], true));
		 }
	 }
	 return $brokers;
}
function pyjack_getListings($class = 'A'){ 
	global $cache;
	
		$key = "listings_".$class;
		$resultsc = $cache->getItem($key);
		$q = '(LIST_15=Active)';
		if (is_null($resultsc->get())) {
	
	    	$rets = pyjack_phrets();
	        $connect = $rets->Login();
	        
	        try{
		         $results = $rets->Search('Property',$class,'*',[
	            'QueryType' => 'DMQL2',
	            'Count' => 1, // count and records
	            'Format' => 'COMPACT-DECODED',
	            'Limit' => 10000,
	            'StandardNames' => 0, // give system names
				]);
	        
				if($results->getTotalResultsCount()){
			        $brokers = pyjack_getAdvertisedBrokers();
			        $listings = array();
			        foreach ($results as $record) {
						$record = $record->toArray();
						foreach($brokers as $broker){
							if($broker['OFFICE_0'] == $record['LIST_106'] && $record['LIST_15'] == 'Active'){
								 $objects = $rets->GetObject('Property', '640x480', $record['LIST_105'], '*', 1);
								 $imgs = array();
								 foreach($objects as $object){
									array_push($imgs, $object->getLocation());
						         }
						         $record['img'] = $imgs;
								 array_push($listings, $record);
							}
						}
					}
					
					$resultsc->set($listings)->expiresAfter(3600);
			        $cache->save($resultsc);
		        }
		    } catch (Exception $e) {
		        
		    }
	       
        }else{
	        $listings = $resultsc->get();
	        	
        }
    
       
       		return $listings;
}  

function pyjack_getRecord($record, $class){ 
	global $cache;

	    $key = "record_".$record;
		$resultsc = $cache->getItem($key);
		
		if (is_null($resultsc->get())) {
	
	    	$rets = pyjack_phrets();
	        $connect = $rets->Login();
	        $query = "(LIST_105=".$record.")";
	        
	        try{
		        
	        $results = $rets->Search('Property',$class,$query,[
	            'QueryType' => 'DMQL2',
	            'Count' => 1, // count and records
	            'Format' => 'COMPACT-DECODED',
	            'Limit' => 1,
	            'StandardNames' => 0, // give system names
	        ]);
	        if($results->getTotalResultsCount()){
		        foreach ($results as $record) {
	    			$record = $record->toArray();
	    			$objects = $rets->GetObject('Property', '640x480', $record['LIST_105'], '*', 1);
	    			        $imgs = array();
							 foreach($objects as $object){
								array_push($imgs, $object->getLocation());
					         }
					         $record['img'] = $imgs;
	
	    			break;
				}
				$resultsc->set($record)->expiresAfter(3600);
			
		        $cache->save($resultsc);
	        }
	         } catch (Exception $e) {
		        
		    }
		    
		    
        }else{
	        $record = $resultsc->get();
	        	
        }
        
       
       		return $record;
} 


	function pyjack_getBrokers(){
      global $cache;
		$key = "brokers";
		$resultsc = $cache->getItem($key);
		if (is_null($resultsc->get())) {
			
	    	$rets = pyjack_phrets();
	        $connect = $rets->Login();
	        $results = $rets->Search('Office','Office','*',[
	            'QueryType' => 'DMQL2',
	            'Count' => 1, // count and records
	            'Format' => 'COMPACT-DECODED',
	            'Limit' => 99999999,
	            'StandardNames' => 0, // give system names
	        ]);
	        
    		$brokers = array();
    		foreach ($results as $record) {
    		    $record = $record->toArray();
    		    $objects = $rets->GetObject('Office', 'Photo', $record['OFFICE_0'], '*', 1);
    			foreach($objects as $object){
    			      $record['img'] = $object->getLocation();
    			      break;
    			}
    			array_push($brokers, $record);
    	    }
    	    $resultsc->set($brokers)->expiresAfter(3600);
    	    $cache->save($resultsc);
        }else{
    	    $brokers = $resultsc->get();
        }
        return $brokers;
	}
	
function renderBrokerOptions($field){ 
    $brokers = pyjack_getBrokers();
    $selected_value = json_decode($field['value'], true);
    foreach($brokers as $record){
         $value = htmlentities(json_encode($record));
        ?><option <?php if($selected_value['OFFICE_0'] == $record['OFFICE_0']) echo 'selected="selected"'; ?> value="<?php echo $value; ?>"><?php echo $record['OFFICE_2']; ?></option><?php
    }
     
}  


function pyjack_broker_title($post_id){
    global $post; 
    global $cache;
    if ($post->post_type != 'pyjack_brokers') return;
    remove_action( 'save_post', 'pyjack_broker_title' );
    $record = json_decode(get_field('broker', $post->ID), true); 
  
    wp_update_post( array( 'ID' => $post->ID, 'post_title' => $record['OFFICE_2'] ) );
    $cache->clear();
    add_action( 'save_post', 'pyjack_broker_title' ); 
}

function pyjack_menu(){
	global $submenu;
	$submenu['pyjack'][] = array( 'Manage Featured Listings', 'manage_options', 'pyjack' );
}
 
function pyjack_acf_settings_path( $path ) {
 $path = plugins_url( '/rets', dirname(__FILE__) ) . '/acf/';
 return $path;
}

function pyjack_acf_settings_dir( $dir ) {
  $dir = plugins_url( '/rets', dirname(__FILE__) ) . '/acf/';
  return $dir;
}

function pyjack_activation() {
    if (! wp_next_scheduled ( 'pyjack_cache' )) {
	//wp_schedule_event(time(), 'hourly', 'pyjack_cache');
    }
}


function pyjack_deactivation() {
	//wp_clear_scheduled_hook('pyjack_cache');
}


function pyjack_cache() {
	global $cache;
	$cache->clear();
	
	
	  error_log('Run Cache! tk!');
	
	pyjack_getListings('A');
	pyjack_getListings('B');
	pyjack_getListings('C');
	pyjack_getListings('D');
	pyjack_getListings('E');
	pyjack_getListings('F');
	pyjack_getListings('G');
	pyjack_getListings('H');
	pyjack_getListings('I');
	pyjack_getBrokers();

}


function pyjack_gold(){
    $gold = get_field('featured_gold', 'option');
    return pyjack_getRecord($gold[0]['mls_#'], $gold[0]['class']);
    
  
}

function pyjack_silver(){
    $silver = array();
    $featured_silver = get_field('featured_silver', 'option');
    foreach($featured_silver as $key => $value) array_push($silver, pyjack_getRecord($value['mls_#'], $value['class']) );
    
    return $silver;
    
  
}


function pyjack_allListings(){
    $all = array();
    
    $a = pyjack_getListings('A');
    $c = pyjack_getListings('C');
    $d = pyjack_getListings('D');
    $i = pyjack_getListings('I');
     
     if(count($a)) foreach ($a as $record) array_push($all, $record);
     if(count($c)) foreach ($c as $record) array_push($all, $record);
     if(count($d)) foreach ($d as $record) array_push($all, $record);
     if(count($i)) foreach ($i as $record) array_push($all, $record);
     
     return $all;
    
}



<?php
/**
 * Plugin Name: Pyjack - flexmls RETS Wordpress plugin for use with broker advertising
 * Plugin URI: http://www.terrifickid.net
 * Description: Wordpress Plugin - RETS feed with list
 * Version: 1.0.0
 * Author: msplint
 * Author URI: http://www.terrifickid.net

 
                  _ _       _            
                 | (_)     | |           
 _____  ___ _ __ | |_ _ __ | |_  
|     \/ __| '_ \| | | '_ \| __/ 
| | | |\__ \ |_) | | | | | | |    
|_|_|_|____/ .__/|_|_|_| |_\__\  
           | |                           
           |_|   
    
  */
require(plugin_dir_path( __FILE__ ).'/vendor/autoload.php');
require(plugin_dir_path( __FILE__ ).'/helper.php');
require( plugin_dir_path( __FILE__ ) . '/acf/acf.php' );
require( plugin_dir_path( __FILE__ ) . '/acf-BROKER/acf-BROKER.php' );
require( plugin_dir_path( __FILE__ ) . '/acf_fields.php' );
require( plugin_dir_path( __FILE__ ) . '/pyjack_brokers.php' ); 
require( plugin_dir_path( __FILE__ ) . '/pyjack_listings.php' );    


//add_filter('acf/settings/show_admin', '__return_false');
add_filter('acf/settings/dir', 'pyjack_acf_settings_dir');
add_filter('acf/settings/path', 'pyjack_acf_settings_path');

add_action( 'acf/init', 'pyjack_fields' );
add_action( 'wp_enqueue_scripts', 'pyjack_queue' );
add_action('save_post','pyjack_broker_title');
add_action( 'admin_menu', 'pyjack_menu' );
add_action('pyjack_cache', 'pyjack_cache');

add_shortcode( 'rets_listing', 'pyjack_code_listing' );
add_shortcode( 'featured_home', 'pyjack_code_featured' );
add_shortcode( 'rets_listing_all', 'pyjack_code_all' );

register_activation_hook(__FILE__, 'pyjack_activation');
register_deactivation_hook(__FILE__, 'pyjack_deactivation');

acf_add_options_page(array(
        'page_title' 	=> 'Manage Featured Listings',
        'menu_title'	=> 'RETS Ads',
        'menu_slug' 	=> 'pyjack',
        'capability'	=> 'edit_posts',
        'redirect'		=> false,
        'position' => 25,
        'icon_url' => 'dashicons-palmtree',
    ));
    











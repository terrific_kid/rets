<?php
if ( ! function_exists('pyjack_listings') ) {

    // Register Custom Post Type
    function pyjack_listings() {

        $labels = array(
            'name'                  => _x( 'Listings', 'Post Type General Name', 'text_domain' ),
            'singular_name'         => _x( 'Listing', 'Post Type Singular Name', 'text_domain' ),
            'menu_name'             => __( 'Listings', 'text_domain' ),
            'name_admin_bar'        => __( 'Listing', 'text_domain' ),
            'archives'              => __( 'Listing Archives', 'text_domain' ),
            'attributes'            => __( 'Listing Attributes', 'text_domain' ),
            'parent_item_colon'     => __( 'Parent Listing:', 'text_domain' ),
            'all_items'             => __( 'All Listings', 'text_domain' ),
            'add_new_item'          => __( 'Add New Listing', 'text_domain' ),
            'add_new'               => __( 'Add New', 'text_domain' ),
            'new_item'              => __( 'New Listing', 'text_domain' ),
            'edit_item'             => __( 'Edit Listing', 'text_domain' ),
            'update_item'           => __( 'Update Listing', 'text_domain' ),
            'view_item'             => __( 'View Listing', 'text_domain' ),
            'view_items'            => __( 'View Listing', 'text_domain' ),
            'search_items'          => __( 'Search Listing', 'text_domain' ),
            'not_found'             => __( 'Not found', 'text_domain' ),
            'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
            'featured_image'        => __( 'Featured Image', 'text_domain' ),
            'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
            'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
            'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
            'insert_into_item'      => __( 'Insert into Listing', 'text_domain' ),
            'uploaded_to_this_item' => __( 'Uploaded to this Listing', 'text_domain' ),
            'items_list'            => __( 'Listing list', 'text_domain' ),
            'items_list_navigation' => __( 'Listing list navigation', 'text_domain' ),
            'filter_items_list'     => __( 'Filter Listing list', 'text_domain' ),
        );
        $args = array(
            'label'                 => __( 'Listing', 'text_domain' ),
            'description'           => __( 'Featured Listings', 'text_domain' ),
            'labels'                => $labels,
            'supports'              => array( ),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => false,
            'menu_position'         => 25,
            'menu_icon'             => 'dashicons-list-view',
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => false,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => true,
            'publicly_queryable'    => true,
            'capability_type'       => 'page',
        );
        register_post_type( 'pyjack_listings', $args );

    }
    add_action( 'init', 'pyjack_listings', 0 );

}
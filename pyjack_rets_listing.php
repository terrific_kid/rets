
<div ng-app="pyjack" ng-cloak class="container-fluid">
	<div ng-controller="listing" class="row">

		
		<div ng-init="load('<?php  echo urlencode(json_encode($listings));  ?>')" ></div>
		
		<div ng-if="single" style="border: 1px solid #ddd; width: 100%;">
			<div style="padding: 1rem;">
				<div class="row">
					<div class="col-sm-6">    
						<slick style="margin-bottom: 1rem;" infinite=true slides-to-show=1 slides-to-scroll=1>
						<div  ng-repeat="img in single.img"><img  src="{{img}}"></div>
						</slick>
					</div>
					<div class="col-sm-6">
						<ul class="pyjack-data">
        		<li><b>{{single.LIST_22 | currency:"$":0}}</b>
        		<li>{{process(single.LIST_31)}} {{process(single.LIST_34)}}, {{process(single.LIST_39)}}, {{process(single.LIST_43)}}</li>
        		</ul>
        		<div class="row">
            		<div class="col-6">
        		<ul class="pyjack-data">
            		<li ng-if="single.LIST_15"><b>Status:</b><br>{{single.LIST_15}}</li>
            		<li ng-if="single.LIST_105"><b>Type:</b><Br>{{single.LIST_9}}</li>
            		<li ng-if="single.LIST_66"><b>Beds:</b><br>{{single.LIST_66}}</li>
            		<li ng-if="single.LIST_67"><b>Baths:</b><br>{{single.LIST_67}}</li>
        		</ul></div>
        		
        		<div class="col-6">
        		<ul  class="pyjack-data">
            		<li ng-if="single.LIST_105"><b>MLS#:</b><br>{{single.LIST_105}}</li>
            		<li ng-if="single.LIST_53"><b>Year Built:</b><br>{{single.LIST_53}}</li>
            		<li ng-if="single.LIST_48"><b>Square Footage:</b><br>{{single.LIST_48}}</li>
            		<li ng-if="single.LIST_56"><b>Lot Acres:</b><br>{{single.LIST_56}}</li>
        		</ul>
        		</div>
        		</div>
        		
        		
        	
        		<!-- Go to www.addthis.com/dashboard to customize your tools --> <div style="margin-bottom: 1rem;" class="addthis_inline_share_toolbox"></div>
        
        							</div>
				</div>
				
				<div class="row">
					<div class="col-12">
					<!-- <button ng-click="isNavCollapsed = !isNavCollapsed"><i class="fa fa-calculator" aria-hidden="true"></i> Calculate Costs</button>
					<button ng-click="brokerListings(single.LIST_106)"><i class="fa fa-search" aria-hidden="true"></i> View Broker Properties</button> -->
					</div>
					<div class="col-12">
						<hr>   
						
						 <slick ng-if="blist" style="margin-bottom: 1rem;" arrows="true" infinite=true slides-to-show=3 slides-to-scroll=1>
						 	<a target="_self" href="/all-listings/#!/{{record.LIST_105}}" style="height: 12rem; overflow: visible; display: block; width: 5rem; background: url('{{safeUrl(record.img[0])}}'); background-size: cover;" ng-repeat="record in blist">
							 	<ul class="pyjack-data smaller">
						 				<li><b>{{record.LIST_22 | currency:"$":0}}</b></li>
						 				<li>{{process(record.LIST_39)}}, {{process(record.LIST_43)}}</li>
						 			</ul>
						 	</a>
						 			
						</slick>
						<hr>
						
						 
						    		
        		<div id="pyjack-calc">
	        		<form method="post" ng-submit="calculate()" >
					  <div class="row">
						  <div class="col-sm-6">
							<div class="form-group">
							<label>Mortgage Term (Years)</label><br>
							<input type="text" class="form-control" ng-model="term" name="mortgage-term" placeholder="30" required>
							</div>
							
							<div class="form-group">
							<label>Interest Rate (Per Year)</label><br>
							<input type="text" class="form-control" ng-model="rate" name="interest-rate" placeholder="5.75" required>
							</div>
							
							<div class="form-group">
							<label>Monthly Payment ($)</label><br>
							<input type="text" class="form-control" ng-model="payment" name="payment" placeholder="" readonly>
							</div>
							
							
						  </div>
						  <div class="col-sm-6">
							<div class="form-group">
							<label>Principal Amount ($)</label><br>
							<input type="text" class="form-control" ng-model="principal" name="principal-amount" placeholder="email@address.com" required>
							</div>
							
							<div class="form-group">
							<label>Down Payment (%)</label><br>
							 <input type="text" class="form-control" ng-model="down" name="down-paymentt" placeholder="email@address.com" required>
							</div>
						  </div>
					  </div><!-- end row -->
					   
					  <div class="form-group">
						  <button>Calculate</button> 
					  </div>
					 
					</form>
				</div>
				<hr>

					</div>
				</div>
    		<div class="row">
	    		
    		<div class="col-sm-6">
	    	 
	    	
	    		 
	    		 <p><b>Description</b></p>
	    		 <p>{{process(single.LIST_78)}}</p>
	    	
	    		  <p ng-if="single.GF20011220161823619649000000">{{single.GF20011220161823619649000000}}</p>
	    		  
	    		 
	    		    
	    		     <p><b>Map</b></p>
	    		     <iframe
  width="100%"
  height="450"
  frameborder="0" style="border:0"
  src="{{mapLink(single)}}" allowfullscreen>
</iframe>
	    		
    		</div>
    		<div class="col-sm-6">
        		
        		<div class="pyjack_pane">
        		<p><b>Contact Agent</b></p>
        		<form method="post" action="<?php echo plugins_url( 'form.php', __FILE__ ); ?>" >
  <div class="form-group">
    <label>First Name</label><br>
    <input type="text" class="form-control" name="First-Name" placeholder="First Name" required>
  </div>
  
  <div class="form-group">
    <label>Last Name</label><br>
    <input type="text" class="form-control" name="Last-Name" placeholder="Last Name" required>
  </div>
  
  <div class="form-group">
    <label>Email</label><br>
    <input type="email" class="form-control" name="Email-Address"placeholder="email@address.com" required>
  </div>
  
    <div class="form-group">
    <label>Message</label><br>
    <textarea class="form-control" name="Message" rows="3"></textarea>
  </div>
  
  <div class="form-group">
	  <button>Submit</button>
  </div>
</form>

        		</div>
            </div>
    		</div>
    		</div>
		</div>
		
		<div ng-if="!single" style="width: 100%;">
		<ul uib-pagination total-items="total" rotate="true" force-ellipses="true" max-size="5" items-per-page="10" ng-model="currentPage" ng-change="flip(currentPage)"></ul>
		<div  ng-repeat="item in listings | limitTo:5:start" class="col-12 pyjack_listing">
			
			
			<div class="row">
				<div style="background-image: url('{{item.img[0]}}');" class="col-sm-6 pyjack_bak">
					 
				</div>
				<div class="col-sm-6">
					<div class="pyjack_pad">
					<ul class="pyjack-data">
						<li ng-if="item.LIST_22"><b>{{item.LIST_22 | currency:"$":0}}</b></li>
						<li ng-if="item.LIST_31">{{process(item.LIST_31)}} {{process(item.LIST_34)}}, {{process(item.LIST_39)}}, {{process(item.LIST_43)}}</li>
						
					</ul>
					
					<ul class="pyjack-data">
						<li ng-if="item.LIST_66">{{item.LIST_66}} bd</li>
						<li ng-if="item.LIST_67">{{item.LIST_67}} ba</li>
						<li ng-if="item.LIST_48">{{item.LIST_48}} sq ft</li>
					</ul>
					
					<div class="row">
						<div class="col-12"><button  ng-click="viewItem(item, true)">View Details</button></div>
					</div>
					
					</div>
				</div>
			</div>
		</div>
			<ul uib-pagination total-items="total" rotate="true" force-ellipses="true" max-size="5" items-per-page="10" ng-model="currentPage" ng-change="flip(currentPage)"></ul>
		</div>
		
	</div>
</div>
<!-- Go to www.addthis.com/dashboard to customize your tools --> 
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-535456f77c0433de"></script> 
	
    
	

        
	
	

	
	